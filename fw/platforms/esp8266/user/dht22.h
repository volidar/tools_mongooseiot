#ifndef DHT22_INCLUDED
#define DHT22_INCLUDED

#include "v7_esp_features.h"

/* This is a library that reads temperature and humidity data from AOSONG DHT22. */

#if V7_ESP_ENABLE__DHT22
/* Reads temperature and humidity data. Returns 1 on success. */
int dht22_read(int pin, double* temperature, double* humidity);
#endif

#endif /* DHT22_INCLUDED */
