/*
 * Copyright (c) 2014-2016 Cesanta Software Limited
 * All rights reserved
 */

#ifndef CS_FW_PLATFORMS_ESP8266_USER_V7_ESP_FEATURES_H_
#define CS_FW_PLATFORMS_ESP8266_USER_V7_ESP_FEATURES_H_

#define V7_ESP_ENABLE__DHT22 1

#endif /* CS_FW_PLATFORMS_ESP8266_USER_V7_ESP_FEATURES_H_ */
