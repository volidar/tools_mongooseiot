#include <osapi.h>
#include <util.h>
#include "v7/v7.h"
#include "common/platforms/esp8266/esp_missing_includes.h"
#include "fw/platforms/esp8266/user/v7_esp_features.h"
#include "fw/platforms/esp8266/user/esp_gpio.h"


#if V7_ESP_ENABLE__DHT22

//ATT Debug Code
//static int lows [128];
//static int highs [128];
//static int pos = 0;

static unsigned int awaitLowHigh(int pin, int* maxCycles) {
  //expecting to start with high, waiting for low
  int highStart = *maxCycles;
  while((*maxCycles)-- > 0 && read_gpio_pin(pin) == 1) ;
  int highLength = highStart - *maxCycles;
  //now it's low, waiting for high
  int lowStart = *maxCycles;
  while((*maxCycles)-- > 0 && read_gpio_pin(pin) == 0) ;
  int lowLength = lowStart - *maxCycles;

  //ATT Debug Code
  //lows[pos] = lowLength;
  //highs[pos] = highLength;
  //pos++;

  return highLength > lowLength;
}

static unsigned int dht22_read_byte(int pin, int* maxCycles) {
  int result = 0, bits = 8;
  while(bits-- > 0) {
    result = result << 1 | awaitLowHigh(pin, maxCycles);
  }
  return result;
}

int dht22_read(int pin, double* temperature, double* humidity) {
  // Send start signal.  See DHT datasheet for full signal diagram:
  //   http://www.adafruit.com/datasheets/Digital%20humidity%20and%20temperature%20sensor%20AM2302.pdf
  sj_gpio_set_mode(pin, GPIO_MODE_OUTPUT, GPIO_PULL_FLOAT);
  set_gpio(pin, 1);
  os_delay_us(250 * 1000);
  set_gpio(pin, 0);
  os_delay_us(20 * 1000);

  int maxCycles = 100000;
  unsigned int b0;
  unsigned int b1;
  unsigned int b2;
  unsigned int b3;
  unsigned int b4;
  { //Time critical part
    set_gpio(pin, 1);
    os_delay_us(40);
    sj_gpio_set_mode(pin, GPIO_MODE_INOUT, GPIO_PULL_PULLUP);
    os_delay_us(10);

    awaitLowHigh(pin, &maxCycles);
    awaitLowHigh(pin, &maxCycles);
    if (maxCycles <= 0) return 0;

    b0 = dht22_read_byte(pin, &maxCycles);
    b1 = dht22_read_byte(pin, &maxCycles);
    b2 = dht22_read_byte(pin, &maxCycles);
    b3 = dht22_read_byte(pin, &maxCycles);
    b4 = dht22_read_byte(pin, &maxCycles);
  }

  *humidity = (b0 * 256 + b1) * 0.1;
  if((b2 & 0x80) == 0) {
    *temperature =  0.1 * ((b2 & 0x7F) * 256 + b3);
  } else {
    *temperature = -0.1 * ((b2 & 0x7F) * 256 + b3); //Not a common encoding of negative numbers. `Sign and magnitude` is used instead of `Two's complement`
  }
  unsigned int expectedCheckSum = b4;
  unsigned int checkSum = (b0 + b1 + b2 + b3) & 0xFF;

  //ATT Debug Code
  //int idx = 0;
  //printf("%d: ", pos);
  //while(idx++ < pos) {
  //  printf("%d/%d ", highs[idx], lows[idx]);
  //  if(idx != 0 && idx % 16 == 0) printf("\r\n");
  //}
  //printf("\r\n");
  //pos = 0;
  //printf("%02X %02X %02X %02X %02X ?%02X h%d t%d @%d\r\n", b0, b1, b2, b3, b4, checkSum, (int)(*humidity * 10.0), (int)(*temperature * 10.0), maxCycles);

  return (maxCycles > 0 && checkSum == expectedCheckSum);
}

#endif /* V7_ESP_ENABLE__DHT22 */
